import path from 'path';
import { defineConfig } from 'vite';

const mode = process.env.NODE_ENV;
const isProd = mode === 'production';
const config = {
  mode,
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd,
    rollupOptions: {
      output: {
        assetFileNames: assetInfo => {
          let [, ext] = assetInfo.name.split('.');

          if ((/png|jpe?g|svg|gif/i).test(ext)) {
            ext = 'img';
          }
          return `${ext}/[name][hash][extname]`;
        },
        entryFileNames: 'js/[name][hash].js'
      }
    }
  },
  resolve: {
    alias: {
      '@react-like': path.resolve(__dirname, 'lib/react-like')
    }
  }
};

export default defineConfig(config);
